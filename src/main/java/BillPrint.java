import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private ArrayList<Play> plays;
	private String customer;
	private ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = plays;
		this.customer = customer;
		this.performances = performances;
	}

	public ArrayList<Play> getPlays() {
		return plays;
	}

	public void setPlays(ArrayList<Play> plays) {
		this.plays = plays;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

	public void setPerformances(ArrayList<Performance> performances) {
		this.performances = performances;
	}

	public String statement() {
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		int totalAmount = 0;
		int volumeCredits = 0;
		String result = "Statement for " + this.customer + "\n";
		for (int i = 0; i < performances.size(); i++) {
			int thisAmount = 0;
			Performance perf = performances.get(i);
			int audience = perf.getAudience();
			String id = perf.getPlayID();
			String name = null;
			String type = null;
			for (int j = 0; j < plays.size(); j++) {
				if (plays.get(i).getId().equals(perf.getPlayID())) {
					name = plays.get(i).getName();
					type = plays.get(i).getType();
					break;
				}
			}
			if (name == null || type == null) {
				throw new IllegalArgumentException("No play found");
			}
			if (type == "tragedy") {
				thisAmount = 40000;
				if (audience > 30) {
					thisAmount += 1000 * (audience - 30);
				}
			} else if (type == "comedy") {
				thisAmount = 30000;
				if (audience > 20) {
					thisAmount += 10000 + 500 * (audience - 20);
				}
				thisAmount += 300 * audience;
			} else {
				throw new IllegalArgumentException("unknown type: " + type);
			}

			// add volume credits
			volumeCredits += Math.max(audience - 30, 0);
			if (type.equals("comedy")) {
				volumeCredits += Math.floor((double) audience / 5.0);
			}

			// print line for this order
			result += "  " + name + ": $" + numberFormat.format((double) thisAmount / 100.00) + " (" + audience
					+ " seats)" + "\n";
			totalAmount += thisAmount;
		}
		result += "Amount owed is $" + numberFormat.format((double) totalAmount / 100) + "\n";
		result += "You earned " + volumeCredits + " credits" + "\n";
		return result;
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}

